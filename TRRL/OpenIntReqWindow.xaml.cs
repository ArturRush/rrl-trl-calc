﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace TRRL
{
	/// <summary>
	/// Логика взаимодействия для OpenIntReqWindow.xaml
	/// </summary>
	public partial class OpenIntReqWindow : Window
	{
		private class ComboItem
		{
			public Surfaces Value{ get; }

			public ComboItem(Surfaces surface)
			{
				Value = surface;
			}

			public override string ToString()
			{
				return Value.Description();
			}
		}

		public Surfaces surfaceType;
		public OpenIntReqWindow()
		{
			InitializeComponent();
			var items = (from object value in Enum.GetValues(typeof (Surfaces)) select new ComboItem((Surfaces) value)).ToList();
			cbSurfaceType.ItemsSource = items;
			if(cbSurfaceType.Items.Count>0)
				cbSurfaceType.SelectedIndex = 0;
		}

		private void cbSurfaceType_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			surfaceType = ((ComboItem) cbSurfaceType.SelectedItem).Value;
		}

		private void confirmButton_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}
	}
}
