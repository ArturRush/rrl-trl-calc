﻿using System;
using System.Collections.Generic;

namespace TRRL
{
	/// <summary>
	/// Базовый класс станций
	/// </summary>
	[Serializable]
	public abstract class Station
	{
		/// <summary>
		/// Название станции
		/// </summary>
		public readonly string Name;
		/// <summary>
		/// Максимальная высота антенн станции
		/// </summary>
		public readonly int MaxAntenHeight;

		/// <summary>
		/// Список поддиапазонов станции
		/// </summary>
		public List<WaveRange> SubRanges = new List<WaveRange>();

		public override string ToString()
		{
			return Name;
		}

		protected Station(string name, int maxAntenHeight)
		{
			Name = name;
			MaxAntenHeight = maxAntenHeight;
		}
	}
}
