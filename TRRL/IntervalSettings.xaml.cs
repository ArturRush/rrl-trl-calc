﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace TRRL
{
	/// <summary>
	/// Логика взаимодействия для IntervalSettings.xaml
	/// </summary>
	public partial class IntervalSettings : UserControl
	{
		private static readonly List<Station> Stations = new List<Station>();

		static IntervalSettings()
		{
			// Создаем необходимые станции
			RRS R409 = new RRS("Р-409", 20);
			R409.SubRanges.Add(new WaveRange("А", 601, 60, 120, 137));
			R409.SubRanges.Add(new WaveRange("Б", 300, 120.2, 239.8, 147));
			R409.SubRanges.Add(new WaveRange("В", 300, 240.4, 479.6, 155));

			TRS R412 = new TRS("Р-412", 5, 38);
			R412.SubRanges.Add(new WaveRange("Первый", 5850, 4438, 4556, 230.5));
			R412.SubRanges.Add(new WaveRange("Второй", 6000, 4630, 4750, 230.5));

			TRS R423 = new TRS("Р-423(ЦИ-48)", 5, 38);
			R423.SubRanges.Add(new WaveRange("Первый", 96, 4435, 4555, 233.9));
			R423.SubRanges.Add(new WaveRange("Второй", 96, 4630, 4750, 233.9));

			RRS R414 = new RRS("Р-414", 30);
			R414.SubRanges.Add(new WaveRange("Диапазон", 46, 1550, 2000, 165.5));

			RRS R415 = new RRS("Р-415", 20);
			R415.SubRanges.Add(new WaveRange("Метровый (Н)", 800, 80, 119.950, 144.0));
			R415.SubRanges.Add(new WaveRange("Дециметровый (В)", 200, 390, 429.8, 155.3));

			RRS R419L1Digital = new RRS("Р-419Л1(ЦИ-480)", 30);
			R419L1Digital.SubRanges.Add(new WaveRange("Первый", 2551, 390, 645, 136.8));
			R419L1Digital.SubRanges.Add(new WaveRange("Второй", 300, 1550, 1850, 146.2));

			Stations.Add(R409);
			Stations.Add(R412);
			Stations.Add(R414);
			Stations.Add(R415);
			Stations.Add(R419L1Digital);
		}

		public IntervalSettings()
		{
			InitializeComponent();
			StationType.ItemsSource = Stations;
		}

		private void StationType_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			Station selected = (Station)StationType.SelectedItem;
			AntennHeight.IsEnabled = true;
			AntennHeight.Value = AntennHeight.Minimum;
			AntennHeight.Maximum = selected.MaxAntenHeight;
			WaveRange.IsEnabled = true;
			WaveRange.ItemsSource = selected.SubRanges;
			WaveNum.Value = WaveNum.Minimum;
			WaveNum.IsEnabled = false;
		}

		private void WaveRange_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (WaveRange.Items.Count > 0)
			{
				WaveNum.Maximum = ((WaveRange)WaveRange.SelectedItem).WavesCount;
				WaveNum.IsEnabled = true;
			}
		}
	}
}
