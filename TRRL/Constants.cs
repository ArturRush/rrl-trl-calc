﻿namespace TRRL
{
	internal static class Constants
	{
		/// <summary> Сглаживание (подобрано эмпирическим путем) </summary>
		public const float Tension = 0.3F;
		/// <summary> Начальное количество точек профиля </summary>
		public const int StartVertexCount = 10;
		/// <summary> Скорость приближения графика </summary>
		public const double ZoomInSpeed = 0.9;
		/// <summary> Скорость отдаления графика </summary>
		public const double ZoomOutSpeed = 1.1;

		/// <summary> 
		/// Теги для кривых на графике
		/// Используются для сортировки по слоям
		/// Чем меньше тег, тем ближе кривая
		/// </summary>
		public static class CurveTags
		{	
			public const int EarthCurveTag = 1;
			public const int ProfileTag = 2;
			public const int ForestsTag = 3;
			public const int StraightLineTag = -4;
			public const int IntersectionPointsTag = -10;
			public const int CriticalGapsCurveTag = -20;
			public const int MirrorCurveTag = -30;
			public const int AngleLineTag = -50;
		}
	}
}
