﻿using System;

namespace TRRL
{
	/// <summary>
	/// Географическая координата (только одна составляющая)
	/// </summary>
    public class GeoCoord
    {
        public int Degrees { get; }
        public int Minutes { get; }
        public int Seconds { get; }

		/// <summary>
		/// Конструктор точки
		/// </summary>
		/// <param name="degrees">Градусы</param>
		/// <param name="minutes">Минуты</param>
		/// <param name="seconds">Секунды</param>
        public GeoCoord(int degrees = 0, int minutes = 0, int seconds = 0)
        {
            Degrees = degrees;
            Minutes = minutes;
            Seconds = seconds;
        }
		
		/// <summary>
		/// Конструктор точки
		/// </summary>
		/// <param name="d">Десятичное представление координаты</param>
		public GeoCoord(double d)
		{
			Degrees = (int) d;
			d -= Degrees;
			d *= 60;
			d = Math.Abs(d);
			Minutes = (int) d;
			d -= Minutes;
			d *= 60;
			Seconds = (int) d;
		}

		/// <summary>
		/// Преобразование географических координат в десятичный вид (только одна составляющая)
		/// </summary>
		/// <returns>Координата в десятичном виде</returns>
        public double ToDouble()
        {
			double temp = Math.Abs(Degrees) + (Minutes * 60 + Seconds) / 3600.0;
			return temp*Math.Sign(Degrees);
        }

		public override string ToString() => $"{Degrees} {Minutes} {Seconds}";

		protected bool Equals(GeoCoord other)
	    {
	        return Degrees == other.Degrees && Minutes == other.Minutes && Seconds == other.Seconds;
	    }

	    public override int GetHashCode()
	    {
	        unchecked
	        {
	            var hashCode = Degrees;
	            hashCode = (hashCode*397) ^ Minutes;
	            hashCode = (hashCode*397) ^ Seconds;
	            return hashCode;
	        }
	    }
    }
}
