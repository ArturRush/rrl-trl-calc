﻿using System.Windows.Controls;

namespace TRRL
{
	public static class ControlsExtensions
	{
		public static void SelectValueByHeader(this ComboBox comboBox, string header)
		{
			foreach (var item in comboBox.Items)
			{
				if (item.ToString() == header)
					comboBox.SelectedItem = item;
			}
		}
	}
}
