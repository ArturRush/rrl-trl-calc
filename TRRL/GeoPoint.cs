﻿using System;

namespace TRRL
{
	/// <summary>
	/// Десятичные координаты
	/// </summary>
	[Serializable]
    public class GeoPoint
    {
        public double Elevation { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

		/// <summary>
		/// Конструктор географической координаты
		/// </summary>
		/// <param name="latitude">Широта</param>
		/// <param name="longitude">Долгота</param>
		/// <param name="elevation">Высота точки</param>
        public GeoPoint(double latitude = 0, double longitude = 0, double elevation = 0)
        {
            Latitude = latitude;
            Longitude = longitude;
            Elevation = elevation;
        }

        public override string ToString() => $"Latitude={Latitude}, Longitude={Longitude}, Elevation={Elevation}";

		/// <summary>
		/// Измерение расстояния по координатам между двумя точками
		/// </summary>
		/// <param name="p1">Первая точка</param>
		/// <param name="p2">Вторая точка</param>
		/// <returns>Расстояние в километрах</returns>
		public static double GetDistance(GeoPoint p1, GeoPoint p2)
		{
			const double R = 6371; // Радиус Земли в километрах
			double cl1 = Math.Cos(ToRadians(p1.Latitude));
			double cl2 = Math.Cos(ToRadians(p2.Latitude));
			double sl1 = Math.Sin(ToRadians(p1.Latitude));
			double sl2 = Math.Sin(ToRadians(p2.Latitude));
			double lonDelta = ToRadians(p2.Longitude - p1.Longitude);
			double cdelta = Math.Cos(lonDelta);
		    double delta = Math.Acos(sl1*sl2 + cl1*cl2*cdelta);
		    return delta*R;
		}

	    private static double ToRadians(double degrees)
	    {
	        return degrees*Math.PI/180;
	    }
	}
}
