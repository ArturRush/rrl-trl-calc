﻿using System.Windows;
using System.Windows.Input;
using Microsoft.Maps.MapControl.WPF;

namespace TRRL
{
	/// <summary>
	/// Interaction logic for MapWindow.xaml
	/// </summary>
	public partial class MapWindow : Window
	{
		public MapWindow()
		{
			InitializeComponent();
		}

		public MapWindow(Location loc1, Location loc2)
		{
			InitializeComponent();
			if (loc1 != null)
			{
				station1.Location = loc1;
				station1.Visibility = Visibility.Visible;
			}
			if (loc2 != null)
			{
				station2.Location = loc2;
				station2.Visibility = Visibility.Visible;
			}
			UpdateLine();
		}

		private void UpdateLine()
		{
			if (station1.Visibility == Visibility.Visible && station2.Visibility == Visibility.Visible)
			{
				line.Locations = new LocationCollection()
				{
					station1.Location,
					station2.Location
				};
			}
		}

		private void OkButton_OnClick(object sender, RoutedEventArgs e)
		{
			Close();
		}
		
		private void MyMap_OnMouseUp(object sender, MouseButtonEventArgs e)
		{
			if (Keyboard.IsKeyDown(Key.LeftCtrl))
			{
				if (e.ChangedButton == MouseButton.Left)
				{
					station1.Visibility = Visibility.Visible;
					station1.Location = myMap.ViewportPointToLocation(e.GetPosition(myMap));
				}
				else if (e.ChangedButton == MouseButton.Right)
				{
					station2.Visibility = Visibility.Visible;
					station2.Location = myMap.ViewportPointToLocation(e.GetPosition(myMap));
				}
				UpdateLine();
			}
		}

		public Location FirstLocation => station1.Location;
		public Location SecondLocation => station2.Location;

		private bool roadMode = true;
		private void ChangeViewButton_Click(object sender, RoutedEventArgs e)
		{
			if (roadMode)
			{
				myMap.Mode = new AerialMode(true);
				roadMode = false;
			}
			else
			{
				myMap.Mode = new RoadMode();
				roadMode = true;
			}
		}
	}
}
