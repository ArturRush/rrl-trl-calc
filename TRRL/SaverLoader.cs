﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace TRRL
{
	public static class SaverLoader
	{
		/// <summary>
		/// Сохранить проект
		/// </summary>
		/// <param name="project">Контейнер с данными проекта</param>
		public static void SaveProject(ProjectContainer project)
		{
			var saveFileDialog = new SaveFileDialog();
			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				var fileName = saveFileDialog.FileName;
				BinaryFormatter formatter = new BinaryFormatter();
				using (var stream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
				{
					try
					{
						formatter.Serialize(stream, project);
						Logger.Log("Файл успешно сохранен", LoggingLevel.Information);
					}
					catch
					{
						Logger.Log("Не удалось сохранить проект", LoggingLevel.Error);
					}
				}
			}
		}

		/// <summary>
		/// Загрузить проект
		/// </summary>
		/// <returns>Контейнер с данными проекта</returns>
		public static ProjectContainer LoadProject()
		{
			var openFileDialog = new OpenFileDialog { Multiselect = false };
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				var fileName = openFileDialog.FileName;
				BinaryFormatter formatter = new BinaryFormatter();
				using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
				{
					try
					{
						var project = (ProjectContainer)formatter.Deserialize(stream);
						return project;
					}
					catch
					{
						Logger.Log("Не удалось загрузить проект", LoggingLevel.Error);
					}
				}
			}
			return null;
		}
	}
}
