﻿using System;
using ZedGraph;

namespace TRRL
{
	static class PointPairExtensions
	{
		public static double DistTo(this PointPair p1, PointPair p2)
		{
			return Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
		}
	}
}
