﻿namespace TRRL
{
	/// <summary>
	/// Типы логгируемой информации
	/// </summary>
    public enum LoggingLevel
    {
        Information,
        Warning,
        Error
    }

    public delegate void LogDelegate(string message, LoggingLevel level);

	/// <summary>
	/// Логгер информации, отображающийся в низу основного окна программы
	/// </summary>
	public static class Logger
    {
        public static event LogDelegate OnLog;

        public static void Log(string message, LoggingLevel level)
        {
            OnLog?.Invoke(message, level);
        }
    }
}
