﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using ZedGraph;

namespace TRRL
{
	public enum Surfaces
	{
		[Description("Водная гладь")]
		Water,
		[Description("Малопересеченные равнины")]
		FreeField,
		[Description("Среднепересеченная открытая местность")]
		OvergrownField,
		[Description("Малопересеченный лес")]
		FreeForest,
		[Description("Среднепересеченный лес")]
		OvergrownForest,
	}

	/// <summary>
	/// Класс, содержащий методы вычислений радиорелейных линий
	/// </summary>
	public static class RrlCalculator
	{
		/// <summary>
		/// Хранит распределение вероятностей глубины замираний на интервалах для различных частот
		/// Соответствие: ГГц - Кривая
		/// </summary>
		public static Dictionary<double, PointPairList> fadingDepth = new Dictionary<double, PointPairList>
		{
			[6] = new PointPairList
				{
					{27.5, 0.1}, {24.7, 0.2}, {23, 0.3}, {21, 0.5}, {19, 0.9}, {16, 2}, {13.5, 4}, {11, 7}, {10, 9}
				},
			[4] = new PointPairList
				{
					{25, 0.1}, {22.2, 0.2}, {20, 0.38}, {19, 0.5}, {16.8, 1}, {14.4, 2}, {13, 3}, {11.9, 4}, {11, 5}, {8.5, 10}
				},
			[2] = new PointPairList
				{
					{22.3, 0.1}, {20, 0.2}, {18, 0.38}, {17, 0.5}, {16, 0.7}, {15, 1}, {12.6, 2}, {13.1, 3}, {9.6, 5}, {8, 8.1}, {7, 10}
				},
			[0.8] = new PointPairList
				{
					{19, 0.1}, {17, 0.2}, {16, 0.3}, {15.2, 0.4}, {15.8, 0.5}, {14, 0.65}, {12.9, 1}, {11, 2}, {10, 3}, {8, 6}, {6.2, 10}
				},
			[0.4] = new PointPairList
				{
					{15, 0.1}, {13.8, 0.2}, {13, 0.3}, {12, 0.5}, {9, 0.8}, {10, 1.5}, {8.6, 3}, {8, 3.8}, {7.2, 5}, {5, 10}
				},
			[0.2] = new PointPairList
				{
					{12.5, 0.1}, {12, 0.15}, {10.9, 0.3}, {10, 0.5}, {9.5, 0.6}, {9, 0.9}, {8.9, 1}, {7, 3}, {6, 5}, {3, 8}, {2.8, 10}
				},
			[0.1] = new PointPairList
				{
					{8.5, 0.1}, {7.5, 0.2}, {7.1, 0.3}, {6.5, 0.5}, {6, 0.75}, {5.5, 1}, {4.8, 2}, {4, 3.5}, {3.5, 5}, {2.7, 10}
				},
			[0] = new PointPairList { { -1, 0.1 }, { -1, 10 } }
		};

		/// <summary>
		/// Значения Ф для вычисления отражающей способности местности в зависимости от типа местности
		/// </summary>
		public static readonly Dictionary<Surfaces, double> SurfaceCoef = new Dictionary<Surfaces, double>()
		{
			{Surfaces.Water,0.999},
			{Surfaces.FreeField, 0.95},
			{Surfaces.FreeForest, 0.9},
			{Surfaces.OvergrownField, 0.7},
			{Surfaces.OvergrownForest, 0.6}
		};

		/// <summary> 
		/// Добавляет ко всем точкам высоту дуги кривизны земной поверхности 
		/// </summary> 
		/// <param name="points">Исходный список точек</param> 
		/// <param name="R">Длина интервала(км)</param> 
		/// <returns>Результирующий список точек</returns> 
		public static PointPairList AddEarthCurve(PointPairList points, double R)
		{
			var pointsWithEarth = new PointPairList();
			double arcHeight = (R / 2.0) * (R / 2.0) / 17;//Высота дуги в середине интервала 
			foreach (PointPair point in points)
			{
				double dist = Math.Abs(point.X - R / 2);//Расстояние от самой высшей точки дуги до текущей 
				pointsWithEarth.Add(new PointPair(point.X, point.Y + (arcHeight - dist * dist / 17)));
			}
			return pointsWithEarth;
		}

		/// <summary> 
		/// Вычитает из всех точек высоту дуги кривизны земной поверхности 
		/// </summary> 
		/// <param name="points">Исходный список точек</param> 
		/// <param name="R">Длина интервала(км)</param> 
		/// <returns>Результирующий список точек</returns> 
		public static PointPairList SubEarthCurve(PointPairList points, double R)
		{
			var pointsWithoutEarth = new PointPairList();
			double arcHeight = (R / 2.0) * (R / 2.0) / 17;//Высота дуги в середине интервала 
			foreach (PointPair point in points)
			{
				double dist = Math.Abs(point.X - R / 2);//Расстояние от самой высшей точки дуги до текущей 
				pointsWithoutEarth.Add(new PointPair(point.X, point.Y - (arcHeight - dist * dist / 17)));
			}
			return pointsWithoutEarth;
		}

		/// <summary>
		/// Возвращает список точек дуги кривизны земной поверхности
		/// </summary>
		/// <param name="R">Длина интервала</param>
		/// <returns>Точки дуги кривизны</returns>
		public static PointPairList GetEarthCurvePoints(double R)
		{
			var earthCurvePoints = new PointPairList();
			const int pointsCnt = 30;
			double arcHeight = (R / 2) * (R / 2) / 17;
			for (int i = 0; i <= pointsCnt; ++i)
			{
				double dist = Math.Abs(R / 2 - i * R / pointsCnt);
				earthCurvePoints.Add(new PointPair(i * R / pointsCnt, arcHeight - dist * dist / 17));
			}
			return earthCurvePoints;
		}

		/// <summary>
		/// Возвращает список точек линии критических просветов. Значения точек необходимо вычесть из высот
		/// </summary>
		/// <param name="R">Длина интервала(км)</param>
		/// <param name="lambda">Длина рабочей волны(метры)</param>
		/// <param name="firstStationHeight">Высота первой станции(без антенны, метры)</param>
		/// <param name="secondStationHeight">Высота второй станции(без антенны, метры)</param>
		/// <returns>Точки кривой критических просветов</returns>
		public static PointPairList GetCriticalGapsPoints(double R, double lambda, double firstStationHeight, double secondStationHeight, double antennHeight)
		{
			const int pointsCnt = 30; //Количество точек для построения кривой критических просветов (вручную строят по 10 точкам)
			var criticalGapsPoints = new PointPairList();
			firstStationHeight += antennHeight;
			secondStationHeight += antennHeight;
			criticalGapsPoints.Add(new PointPair(0, firstStationHeight));//В нулевой точке линия критических просветов начинается от станции
			for (int i = 1; i < pointsCnt; ++i)
			{
				double k = (double)i / pointsCnt;
				double H = Math.Sqrt((1.0 / 3.0) * R * 1000 * lambda * k * (1 - k));//Расстояние кривой крит просветов от линии прямой видимости в метрах
				double straightLine = (secondStationHeight - firstStationHeight) * k + firstStationHeight;//Высота линии прямой видимости в данной точке
				criticalGapsPoints.Add(k * R, straightLine - H);//Расстояние от земли до линии критических просветов в метрах
			}
			criticalGapsPoints.Add(R, secondStationHeight);//Высота линии крит просветов у второй станции
			return criticalGapsPoints;
		}

		/// <summary>
		/// Вычисление величины затухания радиоволн, вносимой рельефом на открытом интервале от плоской поверхности(вычисляется относительно точки отражения)
		/// </summary>
		/// <param name="surface">Тип отражающей поверхности</param>
		/// <param name="h">Величина относительного просвета над точкой отражения в метрах(H/H0, где H0 - расстояние от линии критических просветов до линии прямой видимости, а H - от линии прямой видимости до профиля)</param>
		/// <returns>Значение величины затухания, дБ</returns>
		public static double GetWaveDumpingOpened(Surfaces surface, double h)
		{
			double coef = SurfaceCoef[surface];
			//TODO добавить вычисление отражения от сферических поверхностей
			//Для сферических поверхностей необходимо дополнительно знать ширину и высоту отражающей поверхности, неясно как определять, да и не особо нужно
			return -10 * (Math.Log(1 + coef * coef - 2 * coef * Math.Cos(Math.PI * h * h / 3), 10));
		}

		/// <summary>
		/// Вычисление величины затухания радиоволн, вносимой рельефом на полуоткрытом интервале(вычисляется относительно высшей точки препятствия)
		/// </summary>
		/// <param name="barrier">Препятствие</param>
		/// <param name="R">Длина интервала, км</param>
		/// <returns>Значение величины затухания, дБ</returns>
		public static double GetWaveDumpingHalfOpened(Barrier barrier, double R)
		{
			double k = barrier.DeltaY.X / R;//Относительное расстояние до препятствия
			double y = barrier.DeltaY.Y / 1000.0;//Высота препятствия, км
			double h = barrier.H / barrier.H0;//относительный просвет
			double Rpr = (barrier.Width * barrier.Width) / (y * 8);//Радиус сферы, аппроксимирующей препятствие,км
			double tmp = (R * R * k * k * (1 - k) * (1 - k)) / (Rpr * barrier.H0 / 1000.0);
			double mu = Math.Sign(tmp) * Math.Pow(Math.Abs(tmp), 1.0 / 3.0);//Коэффициент клиновидности
			if (mu < 0.15) mu = 0.15;//В следующей формуле не должно получаться отрицательное число (согласно методичке)
			double wmu = 4 + 10.0 / (mu - 0.1); // Зависимость затухания от клиновидности
			return wmu * (1 - h);
		}

		/// <summary>
		/// Считать ли 2 препятствия как одно?
		/// </summary>
		/// <param name="lambda">Длина волны</param>
		/// <param name="barrier1">Первое препятствие</param>
		/// <param name="barrier2">Второе препятствие</param>
		/// <returns>Одно препятствие или два</returns>
		public static bool IsOneBarrier(double lambda, Barrier barrier1, Barrier barrier2)
		{
			double Rpr1 = (barrier1.Width * barrier1.Width) / (barrier1.HighestPoint.Y / 1000.0 * 8);//Радиус первого препятствия, км
			double Rpr2 = (barrier2.Width * barrier2.Width) / (barrier2.HighestPoint.Y / 1000.0 * 8);//Радиус второго препятствия, км
			double Rpr = Math.Max(Rpr1, Rpr2) / 1000;
			double X = barrier1.X2 + ((barrier2.X1 - barrier1.X2) / 2.0);//Расстояние от вершины более пологого препятствия до места соединения склона с другим препятствием
			double beta = Math.Pow(Rpr * Rpr * lambda, 1.0 / 3.0);
			return X <= beta;
		}

		/// <summary>
		/// Поиск точек пересечения кривой с профилем
		/// </summary>
		/// <param name="profile">Профиль</param>
		/// <param name="curve">Кривая</param>
		/// <param name="curveTension">Сглаживание кривой</param>
		/// <returns>Точки пересечения</returns>
		public static PointPairList GetIntersectionsWithProfile(PointPairList profile, PointPairList curve, float curveTension)
		{
			var intersections = new PointPairList();
			const double step = 0.01;
			//Если первая станция перед стеной - считаем нулевую точку препятствием
			if (profile.SplineInterpolateX(profile.First().X + step, Constants.Tension) >
				curve.SplineInterpolateX(curve.First().X + step, curveTension))
			{
				var tmp = curve.First();
				tmp.X += step/2;
				intersections.Add(tmp);
			}

			double leftX = Math.Max(profile.First().X, curve.First().X) + step;
			double rightX = Math.Min(profile.Last().X, curve.Last().X) - step;
			for (double x = leftX; x <= rightX; x += step)
			{
				double ly1 = profile.SplineInterpolateX(x, Constants.Tension);
				double ly2 = curve.SplineInterpolateX(x, curveTension);
				double ry1 = profile.SplineInterpolateX(x + step, Constants.Tension);
				double ry2 = curve.SplineInterpolateX(x + step, curveTension);
				if ((ly1 - ly2) * (ry1 - ry2) <= 0)
				{
					intersections.Add((x + x + step) / 2.0, curve.SplineInterpolateX((x + x + step) / 2.0, curveTension));
				}
			}
			//Если вторая станция перед стеной - считаем последнюю точку препятствием
			if (intersections.Count % 2 == 1)
			{
				var tmp = curve.Last();
				tmp.X += step / 2;
				intersections.Add(tmp);
			}
			return intersections;
		}

		/// <summary>
		/// Поиск точки отражения для открытых интервалов
		/// </summary>
		/// <param name="profile">Профиль интервала</param>
		/// <param name="tension">Параметр сглаживания профиля</param>
		/// <param name="antennHeight">Высота антенн</param>
		/// <returns></returns>
		public static PointPair GetMirrorPoint(PointPairList profile, float tension, int antennHeight)
		{
			PointPair A = new PointPair(profile.First().X, profile.First().Y + antennHeight);
			PointPair B = new PointPair(profile.Last().X, profile.Last().Y + antennHeight);
			var step = 0.001;
			int valid = 10;//Уменьшаем до 0, если 0, значит участок отражает
						   // Точка отражения берется, если корректно отражается от последовательных 10 точек
						   // Это не сильно обоснованно, но пока так
			PointPair mirrorPoint = null;
			double cx = (A.X + B.X) / 2;
			for (double x = 0; x < (B.X - A.X) * 9 / 10; x += step)
			{
				double y = profile.SplineInterpolateX(x, tension);
				double gamma = Math.Atan2(y - profile.SplineInterpolateX(x - step, tension), step * 1000);
				// Угол наклона профиля в данной точке
				double beta = Math.Atan2(A.Y - y, (x - A.X) * 1000); // Угол на первую антенну
				double alpha = beta + gamma; // Угол падения/отражения
				if (alpha >= Math.PI / 2) continue;
				double omega = alpha + gamma; // Угол отражения относительно оси ОХ
				double endY = y + (B.X - x) * 1000 * Math.Tan(omega); // Ордината конечной точки луча отражения
				if (Math.Abs(endY - B.Y) > 3)
				{
					valid = 10;
					continue; // Если не попадает во вторую антенну
				}
				--valid;
				// Проверка на пересечение с профилем
				if (
					GetIntersectionsWithProfile(profile, new PointPairList(new[] {A.X, x, B.X}, new[] {A.Y, y + 0.01, B.Y}), 0).Count ==
					0 && valid <= 0)
				{
					if(mirrorPoint==null || Math.Abs(x-cx)<Math.Abs(mirrorPoint.X-cx))
						mirrorPoint = new PointPair(x, y);
					valid = 10;
				}
			}
			// Берем точку, наиболее близкую к центру интервала (это больше похоже на правду)
			return mirrorPoint;
		}

		/// <summary>
		/// Расчет допустимого затухания сигнала
		/// </summary>
		/// <param name="R">Длина интервала(км)</param>
		/// <param name="lambda">длина волны(м)</param>
		/// <param name="energyPot">Энергетический потенциал волны</param>
		/// <param name="M">Количество интервалов(1 по умолчанию)</param>
		/// <param name="Hl">Требуемая надежность связи(%)</param>
		/// <returns>Величина допустимого затухания сигнала, дБ</returns>
		public static double GetAcceptableFading(double R, double lambda, double energyPot, int M = 3, int Hl = 97)
		{
			double T1 = (double)(100 - Hl) / M;//Допустимая потеря надежности
			double R0 = 40;//Средняя длина интервала(из методички)
			double qr = GetFadingDepth(T1, lambda) + 20 * Math.Log10(R / R0);
			double Wfree = GetSignalFadingInOpenedSpace(R, lambda);//Величина затухания сигнала в открытом пространстве
			double q0 = energyPot - Wfree;
			return q0 - qr;
		}

		/// <summary>
		/// Допустимая потеря надежности(обычно 1)
		/// </summary>
		/// <param name="T1"></param>
		/// <returns></returns>
		private static double GetFadingDepth(double T1, double lambda)
		{
			//Костыль(типа интерполяция)
			double freq = 299792458.0 / lambda;//Получаем частоту волны
			freq /= 1000000000;//Перевод в ГГц
			if (freq > 6) freq = 5.999;//TODO так быть не должно, но данных пока нет
			KeyValuePair<double, PointPairList> prev = new KeyValuePair<double, PointPairList>(0, fadingDepth[0]);
			foreach (var curve in fadingDepth)
			{
				if (Math.Abs(curve.Key - freq) < 0.001)
				{
					return curve.Value.InterpolateY(T1);
				}
				if (curve.Key > freq)
				{
					var a = curve.Value.InterpolateY(T1);
					var b = prev.Value.InterpolateY(T1);
					return ((freq - prev.Key) / (curve.Key - prev.Key)) * (a - b) + b;
				}
				prev = curve;
			}
			throw new Exception("Интерполирование не удалось");
		}

		/// <summary>
		/// Расчет затухания радиосигнала в открытом пространстве
		/// </summary>
		/// <param name="R">Длина интрервала(км)</param>
		/// <param name="lambda">Длина волны(м)</param>
		/// <returns>Величина затухания радиосигнала</returns>
		public static double GetSignalFadingInOpenedSpace(double R, double lambda)
		{
			return 122 + 20 * Math.Log10(R / (lambda * 100));
		}
	}
}
