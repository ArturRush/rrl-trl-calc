﻿using System;
using System.Collections.Generic;
using ZedGraph;


namespace TRRL
{
	[Serializable]
	public class ProjectContainer
	{
		public readonly GeoPoint FirstStationLocation;
		public readonly GeoPoint SecondStationLocation;
		public PointPairList Profile;
		public List<Forest> Forests;
		public int AntennHeight;
		public Station Station;
		public WaveRange WaveRange;
		public int WaveNum;

		public ProjectContainer(GeoPoint firstStationLocation, GeoPoint secondStationLocation, PointPairList profile, List<Forest> forests, Station station, int antennHeight, WaveRange waveRange, int waveNum)
		{
			FirstStationLocation = firstStationLocation;
			SecondStationLocation = secondStationLocation;
			Profile = profile;
			Forests = forests;
			Station = station;
			AntennHeight = antennHeight;
			WaveRange = waveRange;
			WaveNum = waveNum;
		}
	}
}