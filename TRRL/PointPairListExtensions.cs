﻿using System.Collections.Generic;
using ZedGraph;

namespace TRRL
{
	static class PointPairListExtensions
	{
		public static PointPair First(this IPointList list)
		{
			return list[0];
		}

		public static PointPair Last(this IPointList list)
		{
			return list[list.Count - 1];
		}

		public static PointPairList ToPointPairList(this IEnumerable<PointPair> list)
		{
			var result = new PointPairList();
			foreach (var pointPair in list)
			{
				result.Add(pointPair);
			}
			return result;
		}
	}
}
