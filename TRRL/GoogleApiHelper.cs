﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TRRL
{
    public static class GoogleApiHelper
    {
		/// <summary>
		/// Базовый URL для запроса к google api по профилю высот на отрезке
		/// </summary>
        private const string baseElevationUrl = "https://maps.googleapis.com/maps/api/elevation/json?";
		/// <summary>
		/// API Key гугла
		/// </summary>
        private const string apiKey = "AIzaSyD6XAzDyAWs8fpcfbDU23e3GaBH2o2k81I";
		/// <summary>
		/// Количество точек отрезка, для которых гугл укажет высоту
		/// Не более 499, ограничение бесплатной версии
		/// </summary>
		public const int samplesCount = 20;

		/// <summary>
		/// Получение профиля местности на отрезке по десятичным координатам
		/// </summary>
		/// <param name="start">Начальная точка отрезка</param>
		/// <param name="finish">Конечная точка отрезка</param>
		/// <returns>Массив точек с указанием их десятичных координат и высот в них</returns>
		public static IEnumerable<GeoPoint> GetProfile(GeoPoint start, GeoPoint finish)
        {
            string url = baseElevationUrl +
                         $"path={start.Latitude.ToString(CultureInfo.InvariantCulture)},{start.Longitude.ToString(CultureInfo.InvariantCulture)}|{finish.Latitude.ToString(CultureInfo.InvariantCulture)},{finish.Longitude.ToString(CultureInfo.InvariantCulture)}&samples={samplesCount}&key={apiKey}";
            string json = RequestJson(url);
            IList<JToken> results = JObject.Parse(json)["results"].Children().ToList();
            return results.Select(result => JsonConvert.DeserializeObject<GoogleGeoPoint>(result.ToString()))
                .Select(
                    gp =>
                        new GeoPoint { Elevation = gp.Elevation, Latitude = gp.Location.Lat, Longitude = gp.Location.Lng });
        }

		/// <summary>
		/// Получение json файла из указанного URL-адреса
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
        private static string RequestJson(string url)
        {
            string defaultJson = "{ results: [] }";
            try
            {
                var request = (HttpWebRequest) WebRequest.Create(url);
                var response = (HttpWebResponse) request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    return defaultJson;
                return new StreamReader(response.GetResponseStream()).ReadToEnd();
            }
            catch (Exception)
            {
                Logger.Log("Проблемы с доступом к Интернету. Попробуйте режим \"Без интернета\".", LoggingLevel.Error);
                return defaultJson;
            }
        }

		/// <summary>
		/// Класс-обертка для преобразования полученного json-файла
		/// </summary>
        private class GoogleGeoPoint
        {
            public double Elevation { get; set; }
            public GoogleLocation Location { get; set; }
        }

		/// <summary>
		/// Класс-обертка для преобразования полученного json-файла
		/// </summary>
		private class GoogleLocation
        {
            public double Lat { get; set; }
            public double Lng { get; set; }
        }

	}
}
