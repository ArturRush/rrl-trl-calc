﻿using System;

namespace TRRL
{
	[Serializable]
	public class WaveRange
	{
		/// <summary>
		/// Имя поддиапазона
		/// </summary>
		public readonly string Name;
		/// <summary>
		/// Количество фиксированных волн поддиапазона
		/// </summary>
		public readonly int WavesCount;
		/// <summary>
		/// Минимальная частота поддиапазона (МГц)
		/// </summary>
		public readonly double StartWaveFrequency;
		/// <summary>
		/// Максимальная частота поддиапазона (МГц)
		/// </summary>
		public readonly double EndWaveFrequency;

		public readonly double EnergyPotention;

		/// <summary>
		/// Возвращает частоту волны по ее номеру
		/// </summary>
		/// <param name="waveNum">Номер фиксированной волны</param>
		/// <returns>Частота волны(МГц)</returns>
		public double GetWaveFrequency(int waveNum)
		{
			return StartWaveFrequency + waveNum*((EndWaveFrequency-StartWaveFrequency)/WavesCount);
		}

		/// <summary>
		/// Возвращает длину волны по фиксированному номеру волны
		/// </summary>
		/// <param name="waveNum">Номер волны</param>
		/// <returns>Длина волны, метры</returns>
		public double GetWaveLength(int waveNum)
		{
			return 299792458.0/(GetWaveFrequency(waveNum)*1000000.0);//Используется скорость света, МГц переводятся в Герцы
		}

		/// <summary>
		/// Конструктор диапазона
		/// </summary>
		/// <param name="name">Имя диапазона</param>
		/// <param name="wavesCount">Количество фиксированных волн</param>
		/// <param name="startWaveFrequency">Минимальная частота диапазона</param>
		/// <param name="endWaveFrequency">Максимальная частота диапазона</param>
		public WaveRange(string name, int wavesCount, double startWaveFrequency, double endWaveFrequency, double energyPotention)
		{
			Name = name;
			WavesCount = wavesCount;
			StartWaveFrequency = startWaveFrequency;
			EndWaveFrequency = endWaveFrequency;
			EnergyPotention = energyPotention;
		}

		public override string ToString()
		{
			return Name;
		}
	}
}
