﻿using System;

namespace TRRL
{
	/// <summary>
	/// Радиорелейная станция
	/// </summary>
	[Serializable]
	public class RRS : Station
	{
		/// <summary>
		/// Конструктор радиорелейной станции
		/// </summary>
		/// <param name="name">Название станции</param>
		/// <param name="maxAntenHeight">Максимальная высота антенн станции</param>
		public RRS(string name, int maxAntenHeight) : base(name, maxAntenHeight)
		{
		}
	}
}
