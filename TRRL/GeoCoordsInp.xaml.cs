﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Microsoft.Maps.MapControl.WPF;

namespace TRRL
{
	public partial class GeoCoordsInp
	{
		public GeoCoordsInp()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Название элемента
		/// </summary>
		public string Header
		{
			get { return (string)LblHeader.Content; }
			set { LblHeader.Content = value; }
		}

		/// <summary>
		/// Географическая широта
		/// Елси координаты введены  неверно, то возвращает null, иначе объект GeoCoord
		/// </summary>
		public GeoCoord Latitude
		{
			get
			{
				GeoCoord res;
				if (!TryCoordParse(TxtLatitude.Text, out res))
					return null;
				bool isNorth = RbNorthLat.IsChecked != null && (bool)RbNorthLat.IsChecked;
				if (!isNorth)
					res = new GeoCoord(-res.Degrees, res.Minutes, res.Seconds);
				if (Math.Abs(res.Degrees) > 90 || res.Minutes > 59 || res.Seconds > 59 ||
									(Math.Abs(res.Degrees) == 90 && (res.Minutes > 0 || res.Seconds > 0)))
					return null;
				return res;
			}
			set
			{
				TxtLatitude.Text = $"{Math.Abs(value.Degrees):D3}{value.Minutes:D2}{value.Seconds:D2}";
				TxtLatitude.BorderBrush = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
				if (value.Degrees >= 0)
					RbNorthLat.IsChecked = true;
				else
					RbSouthLat.IsChecked = true;
			}
		}

		/// <summary>
		/// Географическая долгота
		///  Елси координаты введены  неверно, то возвращает null, иначе объект GeoCoord
		/// </summary>
		public GeoCoord Longitude
		{
			get
			{
				GeoCoord res;
				if (!TryCoordParse(TxtLongitude.Text, out res))
					return null;
				bool isEast = RbEastLong.IsChecked != null && (bool)RbEastLong.IsChecked;
				if (!isEast)
					res = new GeoCoord(-res.Degrees, res.Minutes, res.Seconds);
				if (Math.Abs(res.Degrees) > 180 || res.Minutes > 59 || res.Seconds > 59 ||
					(Math.Abs(res.Degrees) == 180 && (res.Minutes > 0 || res.Seconds > 0)))
					return null;
				return res;
			}
			set
			{
				TxtLongitude.Text = $"{Math.Abs(value.Degrees):D3}{value.Minutes:D2}{value.Seconds:D2}";
				TxtLongitude.BorderBrush = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
				if (value.Degrees >= 0)
					RbEastLong.IsChecked = true;
				else
					RbWestLong.IsChecked = true;
			}
		}

		public Location Location
		{
			get
			{
				if (Latitude == null || Longitude == null) return null;
				return new Location(Latitude.ToDouble(), Longitude.ToDouble());
			}
			set
			{
				if (value == null) return;
				Latitude = new GeoCoord(value.Latitude);
				Longitude = new GeoCoord(value.Longitude);
			}
		}

		//TODO: Избавиться от дублирования кода
		/// <summary>
		/// Событие потери фокуса, проверяет правильность введенных данных, подсвечивает красным в случае некорректных данных
		/// </summary>
		private void TxtLatitude_LostFocus(object sender, RoutedEventArgs e)
		{
			TxtLatitude.BorderBrush = Latitude == null ? new SolidColorBrush(Colors.Red) : new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
		}

		/// <summary>
		/// Событие потери фокуса, проверяет правильность введенных данных, подсвечивает красным в случае некорректных данных
		/// </summary>
		private void TxtLongitude_LostFocus(object sender, RoutedEventArgs e)
		{
			TxtLongitude.BorderBrush = Longitude == null ? new SolidColorBrush(Colors.Red) : new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
		}

		/// <summary>
		/// Парсер входной строки текста с маской на географические координаты
		/// </summary>
		/// <param name="s">Входная строка</param>
		/// <param name="coord">Координаты</param>
		/// <returns>Получилось ли распарсить</returns>
		private bool TryCoordParse(string s, out GeoCoord coord)
		{
			coord = new GeoCoord();
			string[] parts = s.Split(new[] { '\'', '°', '"' }, StringSplitOptions.RemoveEmptyEntries);
			if (parts.Length > 0)
				parts[0] = parts[0].TrimStart('_');
			int[] nums = new int[3];
			if (parts.Select((x, i) => int.TryParse(x, out nums[i])).Contains(false))
				return false;
			coord = new GeoCoord(nums[0], nums[1], nums[2]);
			return true;
		}
	}
}
