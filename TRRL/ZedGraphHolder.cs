﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;
using ZedGraph;

namespace TRRL
{
	class ZedGraphHolder
	{
		private readonly ZedGraphControl graphControl;

		private LineItem profileCurve;
		private readonly CurveList forestCurves = new CurveList();
		private LineItem straightLineCurve;
		private LineItem criticalGapsCurve;
		private LineItem earthCurve;
		private LineItem intersectionPointsCurve;
		private LineItem mirrorCurve;
		private LineItem firstAngleLine;
		private LineItem secondAngleLine;
		private List<LineItem> gapsLines = new List<LineItem>();

		private PointPair movedPoint;

		public event Action pointMoved;

		public ZedGraphHolder(ZedGraphControl graphControl)
		{
			this.graphControl = graphControl;
		}

		public void InitGraph()
		{
			// Настройка ZedGraph
			graphControl.GraphPane.Title.Text = "Профиль местности";
			graphControl.GraphPane.XAxis.Title.Text = "Расстояние (км)";
			graphControl.GraphPane.YAxis.Title.Text = "Высота (м)";

			// Для изменения графика с помощью мыши
			graphControl.MouseDownEvent += GraphControlOnMouseDownEvent;
			graphControl.MouseHover += (sender, args) =>
			{
				// Фокусировка при наведении
				graphControl.Focus();
			};
			graphControl.MouseWheel += GraphControlOnMouseWheel;
		}

		public void ClearBesidesProfile()
		{
			straightLineCurve = null;
			criticalGapsCurve = null;
			intersectionPointsCurve = null;
			mirrorCurve = null;
			firstAngleLine = null;
			secondAngleLine = null;
			gapsLines.Clear();
			graphControl.GraphPane.CurveList.Clear();
			graphControl.GraphPane.CurveList.Add(earthCurve);
			graphControl.GraphPane.CurveList.Add(profileCurve);
			foreach (var forestCurve in forestCurves)
			{
				graphControl.GraphPane.CurveList.Add(forestCurve);
			}
			graphControl.Invalidate();
		}

		public void ClearGraph()
		{
			profileCurve = null;
			forestCurves.Clear();
			straightLineCurve = null;
			criticalGapsCurve = null;
			earthCurve = null;
			intersectionPointsCurve = null;
			mirrorCurve = null;
			firstAngleLine = null;
			secondAngleLine = null;
			gapsLines.Clear();
			graphControl.GraphPane.CurveList.Clear();
			graphControl.Invalidate();
		}

		public void DrawEarthCurve(PointPairList earthCurvePoints)
		{
			graphControl.GraphPane.CurveList.Remove(earthCurve);
			earthCurve = graphControl.GraphPane.AddCurve("Дуга кривизны земной поверхности", earthCurvePoints, Color.Blue, SymbolType.None);
			earthCurve.Line.IsSmooth = true;
			earthCurve.Line.SmoothTension = Constants.Tension;
			earthCurve.Line.Fill = new Fill(Color.Blue);
			earthCurve.Tag = Constants.CurveTags.EarthCurveTag;
			UpdateGraph();
		}

		public void DrawProfileCurve(PointPairList profileCurvePoints)
		{
			graphControl.GraphPane.CurveList.Remove(profileCurve);
			profileCurve = graphControl.GraphPane.AddCurve("Профиль", profileCurvePoints, Color.Black, SymbolType.Circle);
			profileCurve.Line.Fill = new Fill(Color.LightGray, Color.DarkSlateGray, 90.0f);
			profileCurve.Line.IsSmooth = true;
			profileCurve.Line.SmoothTension = Constants.Tension;
			profileCurve.Tag = Constants.CurveTags.ProfileTag;
			UpdateGraph();
		}

		public void DrawForests(List<Forest> forests)
		{
			foreach (var forestCurve in forestCurves)
				graphControl.GraphPane.CurveList.Remove(forestCurve);
			forestCurves.Clear();

			foreach (var forest in forests)
			{
				var forestPoints = GetForestPoints(forest);

				var forestCurve = graphControl.GraphPane.AddCurve("Лес", forestPoints, Color.Green, SymbolType.None);
				forestCurve.Line.IsSmooth = true;
				forestCurve.Line.SmoothTension = Constants.Tension;
				forestCurve.Line.Fill = new Fill(Color.Green);
				forestCurve.Label.IsVisible = false;
				forestCurve.Tag = Constants.CurveTags.ForestsTag;
				forestCurves.Add(forestCurve);
			}
			UpdateGraph();
		}

		public void DrawStraightLine(int antennHeight)
		{
			graphControl.GraphPane.CurveList.Remove(straightLineCurve);
			var straightLinePoints = new PointPairList()
			{
				new PointPair(0, profileCurve.Points.First().Y + antennHeight),
				new PointPair(GetProfilePoints().Last().X, GetProfilePoints().Last().Y + antennHeight)
			};
			straightLineCurve = graphControl.GraphPane.AddCurve("Линия прямой видимости", straightLinePoints, Color.Red, SymbolType.None);
			straightLineCurve.Tag = Constants.CurveTags.StraightLineTag;
			UpdateGraph();
		}

		public void DrawCriticalGapsCurve(PointPairList criticalCurvePoints)
		{
			graphControl.GraphPane.CurveList.Remove(criticalGapsCurve);
			criticalGapsCurve = graphControl.GraphPane.AddCurve("Линия критических просветов", criticalCurvePoints, Color.DarkOrange,
				SymbolType.None);
			criticalGapsCurve.Line.IsSmooth = true;
			criticalGapsCurve.Line.SmoothTension = Constants.Tension;
			criticalGapsCurve.Tag = Constants.CurveTags.CriticalGapsCurveTag;
			UpdateGraph();
		}

		public void DrawIntersections(PointPairList intersections)
		{
			graphControl.GraphPane.CurveList.Remove(intersectionPointsCurve);
			intersectionPointsCurve = graphControl.GraphPane.AddCurve("Точки пересечения", intersections, Color.Crimson,
				SymbolType.Diamond);
			intersectionPointsCurve.Line.IsVisible = false;
			intersectionPointsCurve.Tag = Constants.CurveTags.IntersectionPointsTag;
			UpdateGraph();
		}

		public void DrawFirstAngleLine(PointPair firstPoint, int antennHeight)
		{
			graphControl.GraphPane.CurveList.Remove(firstAngleLine);
			var firstHeight = profileCurve.Points.First().Y + antennHeight;
			var distance = profileCurve.Points.Last().X;
			firstAngleLine = graphControl.GraphPane.AddCurve("Первый угол", new PointPairList()
			{
				{0, firstHeight},
				firstPoint,
				{distance, firstHeight + (firstPoint.Y - firstHeight)/firstPoint.X*distance}
			}, Color.DarkMagenta, SymbolType.None);
			firstAngleLine.Label.IsVisible = false;
			firstAngleLine.Tag = Constants.CurveTags.AngleLineTag;
			UpdateGraph();
		}

		public void DrawSecondAngleLine(PointPair secondPoint, int antennHeight)
		{
			graphControl.GraphPane.CurveList.Remove(secondAngleLine);
			var secondHeight = profileCurve.Points.Last().Y + antennHeight;
			var distance = profileCurve.Points.Last().X;
			secondAngleLine = graphControl.GraphPane.AddCurve("Второй угол", new PointPairList()
				{
					{distance, secondHeight},
					secondPoint,
					{0, secondHeight + (secondPoint.Y - secondHeight)/(distance-secondPoint.X)*distance}
				}, Color.DarkMagenta, SymbolType.None);
			secondAngleLine.Label.IsVisible = false;
			secondAngleLine.Tag = Constants.CurveTags.AngleLineTag;
			UpdateGraph();
		}

		public void DrawMirrorCurve(PointPair mirrorPoint, int antennHeight)
		{
			graphControl.GraphPane.CurveList.Remove(mirrorCurve);
			mirrorCurve = graphControl.GraphPane.AddCurve("Луч отражения",
				new PointPairList(new[] { profileCurve.Points.First().X, mirrorPoint.X, profileCurve.Points.Last().X },
				new[] { profileCurve.Points.First().Y + antennHeight, mirrorPoint.Y, profileCurve.Points.Last().Y + antennHeight }),
				Color.DarkOrange);
			mirrorCurve.Tag = Constants.CurveTags.MirrorCurveTag;
			UpdateGraph();
		}

		private PointPairList GetForestPoints(Forest forest)
		{
			var profilePoints = (PointPairList)profileCurve.Points;
			double beginHeight = profilePoints.SplineInterpolateX(forest.Begin, Constants.Tension);
			double endHeight = profilePoints.SplineInterpolateX(forest.End, Constants.Tension);
			var forestPoints = new PointPairList();
			forestPoints.Add(forest.Begin, beginHeight + forest.Height);
			foreach (var profileVertex in profilePoints.Where(p => p.X >= forest.Begin && p.X <= forest.End))
			{
				forestPoints.Add(profileVertex.X, profileVertex.Y + forest.Height);
			}
			forestPoints.Add(forest.End, endHeight + forest.Height);
			return forestPoints;
		}

		private void UpdateGraph()
		{
			UpdateAxis();
			graphControl.GraphPane.CurveList.Sort((c1, c2) =>
			{
				int t1 = (int?)c1.Tag ?? 0;
				int t2 = (int?)c2.Tag ?? 0;
				return t1.CompareTo(t2);
			});
			graphControl.Invalidate();
		}

		private void UpdateAxis()
		{
			// Настройка осей
			graphControl.GraphPane.YAxis.Scale.Min = GetProfilePoints() == null ? 0 : Math.Min(0, GetProfilePoints().Select(p => p.Y).Min());
			graphControl.GraphPane.XAxis.Scale.Min = 0;
			graphControl.GraphPane.XAxis.Scale.Max = GetProfilePoints() == null ? 20 : profileCurve.Points.Last().X;
			graphControl.AxisChange();
		}

		/// <summary>
		/// Расстояние между точками на экране
		/// Сначала переводит точки из zedGrapha в экранные 
		/// </summary>
		private double ScreenDist(PointPair p1, PointPair p2)
		{
			PointF screenP1 = graphControl.GraphPane.GeneralTransform(p1, CoordType.AxisXYScale);
			PointF screenP2 = graphControl.GraphPane.GeneralTransform(p2, CoordType.AxisXYScale);
			return Math.Sqrt(Math.Pow(screenP2.X - screenP1.X, 2) + Math.Pow(screenP2.Y - screenP1.Y, 2));
		}

		public PointPairList GetProfilePoints()
		{
			return (PointPairList)profileCurve?.Points;
		}

		public PointPairList GetEarthPoints()
		{
			return (PointPairList)earthCurve?.Points;
		}

		public PointPairList GetStraightLinePoints()
		{
			return (PointPairList)straightLineCurve?.Points;
		}

		public PointPairList GetCriticalLinePoints()
		{
			return (PointPairList)criticalGapsCurve?.Points;
		}

		public List<PointPairList> GetForests()
		{
			return forestCurves.Select(f => (PointPairList)f.Points).ToList();
		}

		#region Изменение графика профиля с помощью мыши

		private bool GraphControlOnMouseDownEvent(ZedGraphControl sender, MouseEventArgs mouseEventArgs)
		{
			if (profileCurve == null) return false;
			var profileList = (PointPairList)profileCurve.Points;
			PointPair clickPoint = new PointPair();
			graphControl.GraphPane.ReverseTransform(mouseEventArgs.Location, out clickPoint.X, out clickPoint.Y);
			var nearPoint = profileList[0];
			foreach (var point in profileList)
			{
				if (point.DistTo(clickPoint) < nearPoint.DistTo(clickPoint))
					nearPoint = point;
			}
			double projectionY = profileList.SplineInterpolateX(clickPoint.X, Constants.Tension);
			var projection = new PointPair(clickPoint.X, projectionY);
			if (ScreenDist(projection, clickPoint) > 10) return false;
			if (ScreenDist(nearPoint, clickPoint) > 25)
			{
				profileList.Add(clickPoint);
				movedPoint = profileList.Last();
				pointMoved?.Invoke();
				UpdateGraph();
			}
			else
			{
				if (nearPoint.Equals(profileList.First()) || nearPoint.Equals(profileList.Last()))
					movedPoint = null;
				else movedPoint = nearPoint;
			}
			graphControl.MouseMoveEvent += GraphControlOnMouseMoveEvent;
			graphControl.MouseUpEvent += GraphControlOnMouseUpEvent;
			return false;
		}

		private bool GraphControlOnMouseMoveEvent(ZedGraphControl sender, MouseEventArgs mouseEventArgs)
		{
			if (movedPoint == null) return false;
			double x, y;
			graphControl.GraphPane.ReverseTransform(mouseEventArgs.Location, out x, out y);
			// Следим за выходом за границы zedgrapha
			if (x < graphControl.GraphPane.XAxis.Scale.Min || x > graphControl.GraphPane.XAxis.Scale.Max ||
				y < graphControl.GraphPane.YAxis.Scale.Min || y > graphControl.GraphPane.YAxis.Scale.Max)
				return false;
			movedPoint.X = x;
			movedPoint.Y = y;
			pointMoved?.Invoke();
			UpdateGraph();
			return false;
		}

		private bool GraphControlOnMouseUpEvent(ZedGraphControl sender, MouseEventArgs mouseEventArgs)
		{
			movedPoint = null;
			pointMoved?.Invoke();
			UpdateGraph();
			graphControl.MouseMoveEvent -= GraphControlOnMouseMoveEvent;
			graphControl.MouseUpEvent -= GraphControlOnMouseUpEvent;
			return false;
		}

		private void GraphControlOnMouseWheel(object sender, MouseEventArgs args)
		{
			if (profileCurve == null) return;
			for (int i = 0; i < Math.Abs(args.Delta) / SystemInformation.MouseWheelScrollDelta; i++)
			{
				if (args.Delta > 0) // Приближение
				{
					double maxProfileY = 0;
					for (int j = 0; j < profileCurve.Points.Count; j++)
						maxProfileY = Math.Max(maxProfileY, profileCurve.Points[j].Y);
					// Ниже, чем самая высокая точка профиля, не опускаем
					double newYaxis = Math.Max(graphControl.GraphPane.YAxis.Scale.Max * Constants.ZoomInSpeed, maxProfileY);
					graphControl.GraphPane.YAxis.Scale.Max = newYaxis;
				}
				else // Отдаление
				{
					graphControl.GraphPane.YAxis.Scale.Max *= Constants.ZoomOutSpeed;
				}
			}
			graphControl.Invalidate();
		}


		#endregion

		public void DrawGaps(PointPairList gaps)
		{
			var curve = graphControl.GraphPane.AddCurve("", gaps, Color.DarkMagenta, SymbolType.None);
			curve.Line.Style = DashStyle.Custom;
			curve.Line.DashOn = 7.0f;
			curve.Line.DashOff = 50.0f;
			gapsLines.Add(curve);
			UpdateGraph();
		}

		public void DrawSecantPlane(PointPair startPoint, PointPair endPoint)
		{
			graphControl.GraphPane.AddCurve("Секущая плоскость", new PointPairList() {startPoint, endPoint}, Color.Black);
			UpdateGraph();
		}

		public void InitMaxScaleY()
		{
			graphControl.GraphPane.YAxis.Scale.Max = GetProfilePoints() == null 
				? 200 
				: GetProfilePoints().Select(p => p.Y).Max()*2;
			graphControl.AxisChange();
		}
	}
}
