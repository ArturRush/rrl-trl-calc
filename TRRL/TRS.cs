﻿using System;

namespace TRRL
{
	/// <summary>
	/// Тропосферная станция
	/// </summary>
	[Serializable]
	public class TRS : Station
	{
		/// <summary>
		/// Конструктор тропосферной станции
		/// </summary>
		/// <param name="name">Название станции</param>
		/// <param name="maxAntenHeight">Максимальная высота антенн станции</param>
		/// <param name="AnntennGain">Коэффициент усиления антенны</param>
		public TRS(string name, int maxAntenHeight, double AnntennGain) : base(name, maxAntenHeight)
		{
		}

		/// <summary>
		/// Коэффициент усиления антенны
		/// </summary>
		public double AntennGain;
	}
}
