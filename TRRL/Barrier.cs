﻿using ZedGraph;

namespace TRRL
{
	public class Barrier
	{
		public Barrier(double x1, double x2, PointPair highestPoint, double h0, double h, PointPair dY)
		{
			X1 = x1;
			X2 = x2;
			HighestPoint = highestPoint;
			H0 = h0;
			H = h;
			DeltaY = dY;
		}

		/// <summary> Расстояние до начала препятствия </summary>
		public double X1 { get; set; }
		/// <summary> Расстояние до конца препятствия </summary>
		public double X2 { get; set; }
		/// <summary> Высшая точка препятствия </summary>
		public PointPair HighestPoint { get; set; }
		/// <summary> Критический просвет, метры(от линии прямой видимости до линии крит просвета) </summary>
		public double H0 { get; set; }
		/// <summary> Реальный просвет, метры(от линии прямой видимости до профиля) </summary>
		public double H { get; set; }
		/// <summary> Х - расстояние точки от начала координат, У - dy препятствия , метры (максимальное расстояние от секущей до линии профиля препятствия)</summary>
		public PointPair DeltaY { get; set; }
		
		/// <summary> Ширина препятствия </summary>
		public double Width => X2 - X1;

		public override string ToString()
		{
			return $"X1 = {X1}, X2 = {X2}, HighestPoint = ({HighestPoint.X}, {HighestPoint.Y}), H0 = {H0}, H = {H}";
		}
	}
}
