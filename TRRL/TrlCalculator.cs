﻿using System;
using ZedGraph;

namespace TRRL
{
	public static class TrlCalculator
	{
		/// <summary>
		/// Коэф приземного преломления для ЦЕТС(константа на февраль)
		/// </summary>
		private const int N0 = 310;

		/// <summary>
		/// Рассчитывает угол закрытия для станции
		/// </summary>
		/// <param name="profile">Профиль интервала</param>
		/// <param name="tension">Параметр сглаживания кривой профиля</param>
		/// <param name="station">Позиция антенны станции</param>
		/// <param name="point">Точка угла закрытия</param>
		/// <returns>Угол закрытия B(градусы)</returns>
		public static double GetAngleOfClosure(PointPairList profile, float tension, PointPair station, out PointPair point)
		{
			double maxAngle = Double.MinValue;
			point = null;
			PointPair A = new PointPair(profile.First().X, profile.First().Y + station.Y);
			PointPair B = new PointPair(profile.Last().X, profile.Last().Y + station.Y);
			var step = 0.001;
			for (double x = step; x < (B.X - A.X); x += step)
			{
				double y = profile.SplineInterpolateX(x, tension);
				double curAngle = Math.Atan((y - station.Y)/(Math.Abs(x-station.X)*1000));
                if (curAngle > maxAngle)
				{
					maxAngle = curAngle;
					point = new PointPair(x, y);
				}
			}
			Logger.Log("Угол закрытия: " + (maxAngle*180/Math.PI).ToString("0.00") + " град.", LoggingLevel.Information);
			return maxAngle * 180 / Math.PI;
		}

		/// <summary>
		/// Затухание сигнала ТРЛ
		/// </summary>
		/// <param name="R">Длина интервала(км)</param>
		/// <param name="lambda">Длина волны(м)</param>
		/// <param name="htr">Средняя высота интервала</param>
		/// <param name="B">Суммарный угол закрытия</param>
		/// <param name="antennGain">Коэффициент усиления антенн тропосферной станции</param>
		/// <param name="g">Значение вертикального градиента индекса преломления воздуха</param>
		/// <param name="Nz">Индекс преломления воздуха</param>
		/// <returns>Затухание сигнала ТРС на данном интервале</returns>
		public static double GetSignalFading(double R, double lambda, double htr, double B, double antennGain, double g = 0.041, double Nz = 330)
		{
			double Re = R + 148 * B;//Эквивалентное расстояние
			double Wst = (Re < (100 * (1 + Math.Log(lambda*100, Math.E))))//Длина волны переводится в сантиметры
				? 74 + 0.05 * Re - 10 * Math.Log(lambda * 100, Math.E)
				: 64 + 0.15 * Re +
				  (4.3 * Math.Log(lambda * 100, Math.E) - 0.043 * Re - 15.7) * Math.Log(lambda * 100, Math.E);//Стандартное затухание волн + влияние местности
			double k = (lambda < 0.1 ? 0.55 - 0.00078 * Re : 0.9 - 0.0015 * Re);//Коэффициент соответствия, зависит от диапазона частот (сантиметровый\дециметровый)
			double Wcl = -k * (Nz - N0);//Влияние климата на затухание
			double Whtr = 0;//Затухание волн на приподнятых участках
			if (htr >= 200) Whtr = -k * Nz * (Math.Pow(Math.E, (-1 / Nz) * g * (htr - 200)) - 1);
			double Wa = 6 * Math.Pow(10, -9) * Math.Pow((antennGain * 2), 4.73);//Потери усиления антенн в зависимости от коэффициента усиления

			return RrlCalculator.GetSignalFadingInOpenedSpace(R, lambda) + Wst + Wcl + Whtr + Wa;
		}

		/// <summary>
		/// Запас уровня сигнала тропосферной станции
		/// </summary>
		/// <param name="energyPotencial">Энергетический потенциал станции</param>
		/// <param name="signalFading">Затухание сигнала на интервале</param>
		/// <returns>Запас уровня сигнала ТРС на данном интервале</returns>
		public static double GetSignalMargin(double energyPotencial, double signalFading)
		{
			return energyPotencial - signalFading;
		}

		//TODO разобраться со странной херней
		//Есть 2 пути, и они очень разные. Либо считать так, но по номограммам и запас выходит очень маленький по замиранию
		//Либо просто считать, что энергитический потенциал станции и есть приемлимое затухание (следует из некоторых формул в методичке)
		//Сейчас используется второй вариант
		public static double GetAcceptableFading(double energyPotention)
		{
			return energyPotention;
		}

		/// <summary>
		/// Получить среднюю высоту на интервале
		/// </summary>
		/// <param name="profile">Профиль интервала</param>
		/// <returns>Средняя высота профиля</returns>
		public static double GetMediumAltitude(PointPairList profile)
		{
			double tmp = 0;
			foreach (var point in profile)
				tmp += point.X;
			return tmp / profile.Count;
		}
	}
}
