﻿using System;

namespace TRRL
{
	/// <summary>
	/// Представление леса
	/// </summary>
	[Serializable]
	public class Forest
	{
		/// <summary>
		/// Начало леса
		/// </summary>
		public double Begin { get; set; }

		/// <summary>
		/// Конец леса
		/// </summary>
		public double End { get; set; }
		
		/// <summary>
		/// Высота леса(с учетом возраста)
		/// </summary>
		public double Height { get; set; }
	}
}
