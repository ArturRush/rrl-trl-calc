﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using ZedGraph;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using Point = System.Windows.Point;
using RichTextBox = System.Windows.Controls.RichTextBox;

namespace TRRL
{
	// TODO: Сделать считывание станций из файла (?)
	// TODO: Учет климатических районов в расширенных настройках
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private PointPairList profileList;
		private List<Forest> forestList = new List<Forest>();

		public event Action OnProfileListChanged;
		public event Action OnForestListChanged;

		private ZedGraphHolder graphHolder;

		public MainWindow()
		{
			InitializeComponent();
			graphHolder = new ZedGraphHolder(graphControl);
			graphHolder.InitGraph();

			Logger.OnLog += WriteMessageToTextbox;

			OnProfileListChanged += OnProfileUpdate;
			graphHolder.pointMoved += OnProfileUpdate;

			OnForestListChanged += () =>
			{
				lwForest.Items.Clear();
				foreach (var forest in forestList)
					lwForest.Items.Add(forest);
				graphHolder.DrawForests(forestList);
			};
		}

		private void OnProfileUpdate()
		{
			profileList.Sort(SortType.XValues);
			// Обновляем listView с точками профиля
			lwProfile.Items.Clear();
			// Первую и последнюю точку не выводим, чтобы не было возможности изменить
			for (int i = 1; i < profileList.Count - 1; ++i)
			{
				lwProfile.Items.Add(new Point(profileList[i].X, profileList[i].Y));
			}

			graphHolder.DrawProfileCurve(profileList);
			graphHolder.DrawForests(forestList);
		}


		private void BtnBuildProfile_OnClick(object sender, RoutedEventArgs e)
		{
			graphHolder.ClearGraph();
			if (tabOnline.IsSelected)
				BtnBuildProfileOnline_OnClick(sender, e);
			else BtnBuildProfileOffline_OnClick(sender, e);
			graphHolder.InitMaxScaleY();
		}

		private void BtnBuildProfileOnline_OnClick(object sender, RoutedEventArgs e)
		{
			//#if (DEBUG)
			//			var latStart = new GeoCoord(55, 55, 55);
			//			var lngStart = new GeoCoord(55, 55, 55);
			//			var latFinish = new GeoCoord(56, 55, 55);
			//			var lngFinish = new GeoCoord(56, 55, 55);
			//#else
			var latStart = gciFirstStation.Latitude;
			var lngStart = gciFirstStation.Longitude;
			var latFinish = gciSecondStation.Latitude;
			var lngFinish = gciSecondStation.Longitude;
			//#endif
			if (latStart == null || lngStart == null || latFinish == null || lngFinish == null)
			{
				Logger.Log("Неверно введены координаты", LoggingLevel.Error);
				return;
			}
			if (latStart.Equals(latFinish) && lngStart.Equals(lngFinish))
			{
				Logger.Log("Начальная и конечная точка не должны совпадать", LoggingLevel.Error);
				return;
			}
			var startPoint = new GeoPoint(latStart.ToDouble(), lngStart.ToDouble());
			var finishPoint = new GeoPoint(latFinish.ToDouble(), lngFinish.ToDouble());
			var points = GoogleApiHelper.GetProfile(startPoint, finishPoint).ToList();
			if (points.Count == 0) return;
			profileList =
				points.Select(point => new PointPair(GeoPoint.GetDistance(point, startPoint), point.Elevation)).ToPointPairList();
			forestList.Clear();
			RebuildGraph();
			OnProfileListChanged?.Invoke();
			OnForestListChanged?.Invoke();
		}

		private void BtnBuildProfileOffline_OnClick(object sender, RoutedEventArgs e)
		{
			if (iudIntervalLength.Value == null || iudFirstStationHeight.Value == null || iudSecondStationHeight.Value == null)
			{
				Logger.Log("Заданы не все параметры для построения профиля.", LoggingLevel.Error);
				return;
			}
			int intervalLen = iudIntervalLength.Value.Value;
			int firstStationHeight = iudFirstStationHeight.Value.Value;
			int secondStationHeight = iudSecondStationHeight.Value.Value;
			profileList = new PointPairList();
			profileList.Add(0, firstStationHeight);
			for (int i = 1; i < Constants.StartVertexCount; ++i)
				profileList.Add((double)i / Constants.StartVertexCount * intervalLen,
					firstStationHeight + (secondStationHeight - firstStationHeight) * (double)i / 10);
			profileList.Add(intervalLen, secondStationHeight);
			forestList.Clear();
			RebuildGraph();
			OnProfileListChanged?.Invoke();
			OnForestListChanged?.Invoke();
		}

		/// <summary>
		/// Перестроение графика
		/// </summary>
		private void RebuildGraph()
		{
			graphHolder.DrawEarthCurve(RrlCalculator.GetEarthCurvePoints(profileList.Last().X - profileList.First().X));

			// Рисуем линию профиля
			profileList = RrlCalculator.AddEarthCurve(profileList, profileList.Last().X - profileList.First().X);
			graphHolder.DrawProfileCurve(profileList);

			btnAddVertex.IsEnabled = true;
			btnAddForest.IsEnabled = true;
			dudVertexDist.Minimum = 0.1;
			dudVertexDist.Maximum = profileList.Last().X - 0.1;
			dudForestBegin.Minimum = dudForestEnd.Minimum = 0;
			dudForestBegin.Maximum = dudForestEnd.Maximum = profileList.Last().X;

			Logger.Log("Профиль построен. Теперь его можно редактировать.", LoggingLevel.Information);
		}

		/// <summary>
		/// Привязка нажатия Enter к кнопке "Рассчитать"
		/// </summary>
		private void Window_KeyUp(object sender, KeyEventArgs e)
		{
			if (e.Key != Key.Enter || !tabs.IsKeyboardFocusWithin) return;
			BtnBuildProfile_OnClick(sender, e);
		}

		private void BtnAddVertex_Click(object sender, RoutedEventArgs e)
		{
			// Новую точку по умолчанию добавляем в середину самого длинного отрезка
			var index = 0;
			for (int i = 1; i < profileList.Count - 1; ++i)
				if (profileList[i + 1].X - profileList[i].X > profileList[index + 1].X - profileList[index].X)
					index = i;
			var newPoint = new PointPair((profileList[index].X + profileList[index + 1].X) / 2.0,
				(profileList[index].Y + profileList[index + 1].Y) / 2.0);
			profileList.Add(newPoint);
			OnProfileListChanged?.Invoke();
			lwProfile.SelectedIndex = index; // не index+1, потому что в lwProfile не выводится первая точка из profileList

			Logger.Log($"Добавлена точка профиля ({newPoint.X}, {newPoint.Y}).", LoggingLevel.Information);
		}

		private void BtnDelVertex_Click(object sender, RoutedEventArgs e)
		{
			if (lwProfile.SelectedIndex == -1) return;
			profileList.RemoveAt(lwProfile.SelectedIndex + 1);
			OnProfileListChanged?.Invoke();
		}

		private void BtnAddForest_OnClick(object sender, RoutedEventArgs e)
		{
			forestList.Add(new Forest());
			OnForestListChanged?.Invoke();
			Logger.Log("Добавлен лес. Задайте его параметры с помощью панели редактирования справа.", LoggingLevel.Information);
		}

		private void BtnDelForest_OnClick(object sender, RoutedEventArgs e)
		{
			if (lwForest.SelectedIndex == -1) return;
			forestList.RemoveAt(lwForest.SelectedIndex);
			OnForestListChanged?.Invoke();
		}

		private void LwProfile_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			bool editEnabled = lwProfile.SelectedItem != null;
			btnDelVertex.IsEnabled = editEnabled;
			iudVertexHeight.IsEnabled = editEnabled;
			dudVertexDist.IsEnabled = editEnabled;
			if (lwProfile.SelectedItem != null)
			{
				var item = (Point)lwProfile.SelectedItem;
				dudVertexDist.ValueChanged -= ProfileVertexChanged;
				iudVertexHeight.ValueChanged -= ProfileVertexChanged;
				dudVertexDist.Value = item.X;
				iudVertexHeight.Value = (int)Math.Round(item.Y);
				dudVertexDist.ValueChanged += ProfileVertexChanged;
				iudVertexHeight.ValueChanged += ProfileVertexChanged;
			}
		}

		private void LwForest_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			bool editEnabled = lwForest.SelectedItem != null;
			btnDelForest.IsEnabled = editEnabled;
			dudForestBegin.IsEnabled = editEnabled;
			dudForestEnd.IsEnabled = editEnabled;
			iudForestHeight.IsEnabled = editEnabled;
			if (lwForest.SelectedItem != null)
			{
				var item = (Forest)lwForest.SelectedItem;
				dudForestBegin.ValueChanged -= ForestChanged;
				dudForestEnd.ValueChanged -= ForestChanged;
				iudForestHeight.ValueChanged -= ForestChanged;
				dudForestBegin.Value = item.Begin;
				dudForestEnd.Value = item.End;
				iudForestHeight.Value = (int)Math.Round(item.Height);
				dudForestBegin.ValueChanged += ForestChanged;
				dudForestEnd.ValueChanged += ForestChanged;
				iudForestHeight.ValueChanged += ForestChanged;
			}
		}

		private void ProfileVertexChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			if (lwProfile.SelectedIndex == -1) return;
			if (dudVertexDist.Value == null || iudVertexHeight.Value == null) return;
			int index = lwProfile.SelectedIndex + 1;
			profileList[index].X = dudVertexDist.Value.Value;
			profileList[index].Y = iudVertexHeight.Value.Value;
			var temp = profileList[index];
			OnProfileListChanged?.Invoke();
			int newIndex = profileList.FindIndex(p => p.Equals(temp));
			lwProfile.SelectedIndex = newIndex - 1;
		}

		private void ForestChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			if (lwForest.SelectedIndex == -1) return;
			if (dudForestBegin.Value == null || dudForestEnd.Value == null || iudForestHeight.Value == null) return;
			int index = lwForest.SelectedIndex;
			forestList[index].Begin = dudForestBegin.Value.Value;
			forestList[index].End = dudForestEnd.Value.Value;
			forestList[index].Height = iudForestHeight.Value.Value;
			OnForestListChanged?.Invoke();
			lwForest.SelectedIndex = index;
		}

		#region Расчет

		private void BtnCalculate_OnClick(object sender, RoutedEventArgs e)
		{
			graphHolder.ClearBesidesProfile();
			if (graphHolder.GetProfilePoints() == null)
			{
				Logger.Log("Перед расчетом необходимо построить профиль.", LoggingLevel.Error);
				return;
			}
			var profileWithForest = MergeProfileWithForest();
			if (intervalSettings.StationType == null || intervalSettings.AntennHeight.Value == null ||
					intervalSettings.WaveNum.Value == null ||
					intervalSettings.WaveRange.SelectedItem == null)
			{
				Logger.Log("Заданы не все параметры для расчета.", LoggingLevel.Error);
				return;
			}
			double distance = profileList.Last().X;
			var range = (WaveRange)intervalSettings.WaveRange.SelectedItem;
			var lambda = range.GetWaveLength(intervalSettings.WaveNum.Value.Value);

			if (intervalSettings.StationType.SelectedItem is RRS) // Расчет РРС
			{
				CalculateRRL(lambda, profileWithForest, distance, range);
			}
			else // Расчет ТРС
			{
				CalculateTRL(lambda, profileWithForest, distance, range);
			}
		}

		private void CalculateRRL(double lambda, PointPairList profileWithForest, double distance, WaveRange range)
		{
			// Рисуем линию прямой видимости
			var antennHeight = intervalSettings.AntennHeight.Value.Value;
			graphHolder.DrawStraightLine(antennHeight);

			// Рисуем линию критических просветов
			var criticalGapsPoints = RrlCalculator.GetCriticalGapsPoints(profileList.Last().X - profileList.First().X, lambda,
				profileList.First().Y, profileList.Last().Y, (int)intervalSettings.AntennHeight.Value);
			graphHolder.DrawCriticalGapsCurve(criticalGapsPoints);

			// Поиск и отрисовка точек пересечения
			var intersections = RrlCalculator.GetIntersectionsWithProfile(profileWithForest, graphHolder.GetCriticalLinePoints(), Constants.Tension);
			graphHolder.DrawIntersections(intersections);

			var barriers = GetBarriers(intersections);

			var straightLineIntersections = RrlCalculator.GetIntersectionsWithProfile(profileWithForest, graphHolder.GetStraightLinePoints(), 0);
			string intervalType = straightLineIntersections.Count > 0 ? "закрытый" : (intersections.Count > 0 ? "полузакрытый" : "открытый");
			double fading = barriers.Count > 0
				? (straightLineIntersections.Count > 0
					? CalculateFadingOnClosedInterval(lambda, profileWithForest, barriers, distance)
					: CalculateFadingOnHalfOpenedInterval(barriers, distance))
				: CalculateFadingOnOpenedInterval();
			double acceptableFading = RrlCalculator.GetAcceptableFading(distance, lambda, range.EnergyPotention);
			Logger.Log("Тип интервала: " + intervalType + ".\r" +
				(acceptableFading - fading > 0 ? "Интервал пригоден\r" : "Интервал непригоден.\r") +
				"Величина затухания сигнала на интервале: " + fading.ToString("0.00") + " дБ.\rДопустимое затухание сигнала: " +
				acceptableFading.ToString("0.00") + " дБ.\r" + "Запас сигнала: " + (acceptableFading - fading).ToString("0.00") +
				" дБ.", LoggingLevel.Information);
		}

		/// <summary>
		/// Расчет затухания на закрытом интервале
		/// </summary>
		/// <param name="lambda">Длина волны</param>
		/// <param name="profileWithForest">Профиль, объединенный с лесом</param>
		/// <param name="barriers">Препятствия</param>
		/// <param name="distance">Длина интервала</param>
		/// <returns>Величина затухания</returns>
		private double CalculateFadingOnClosedInterval(double lambda, PointPairList profileWithForest, List<Barrier> barriers, double distance)
		{
			int antennHeight = intervalSettings.AntennHeight.Value.Value;
			var firstStation = new PointPair(0, profileList.First().Y + antennHeight);
			PointPair firstPoint;
			TrlCalculator.GetAngleOfClosure(profileWithForest, Constants.Tension, firstStation, out firstPoint);
			graphHolder.DrawFirstAngleLine(firstPoint, antennHeight);

			var secondStation = new PointPair(distance, profileList.Last().Y + antennHeight);
			PointPair secondPoint;
			TrlCalculator.GetAngleOfClosure(profileWithForest, Constants.Tension, secondStation, out secondPoint);
			graphHolder.DrawSecondAngleLine(secondPoint, antennHeight);

			double gaps1EndHeight = firstStation.Y + (firstPoint.Y - firstStation.Y) / firstPoint.X * distance;
			PointPairList gaps1 = RrlCalculator.GetCriticalGapsPoints(distance, lambda, firstStation.Y, gaps1EndHeight, 0);
			graphHolder.DrawGaps(gaps1);

			double gaps2BeginHeight = secondStation.Y + (secondPoint.Y - secondStation.Y) / (distance - secondPoint.X) * distance;
			PointPairList gaps2 = RrlCalculator.GetCriticalGapsPoints(distance, lambda, gaps2BeginHeight, secondStation.Y, 0);
			graphHolder.DrawGaps(gaps2);

			PointPair E = RrlCalculator.GetIntersectionsWithProfile(profileWithForest, gaps1, 0).First();
			PointPair F = RrlCalculator.GetIntersectionsWithProfile(profileWithForest, gaps2, 0).Last();
			graphHolder.DrawSecantPlane(E, F);

			double k1 = (firstPoint.Y - firstStation.Y) / firstPoint.X;
			double k2 = (secondPoint.Y - secondStation.Y) / (secondPoint.X - distance);
			double y01 = firstStation.Y;
			double y02 = secondStation.Y - k2 * distance;
			double mx = (y01 - y02) / (k2 - k1);
			double my = y01 + k1 * mx;
			PointPair M = new PointPair(mx, my);

			double H = graphHolder.GetStraightLinePoints().InterpolateX(M.X) - M.Y;
			double H0 = M.Y - graphHolder.GetCriticalLinePoints().InterpolateX(M.X);

			PointPairList sectantPlane = new PointPairList() { E, F };
			// TODO По идее надо искать максимальную разницу между точкой профиля и секущей плоскость, но пока беру от точек касания, ибо лень:)
			double deltaY = Math.Max(firstPoint.Y - sectantPlane.InterpolateX(firstPoint.X),
				secondPoint.Y - sectantPlane.InterpolateX(secondPoint.X));
			double Lpr = F.X - E.X;
			double Rpr = Lpr * Lpr / (8 * deltaY / 1000);
			double k = M.X / distance;
			double mu = Math.Pow(distance * distance * k * k * (1 - k) * (1 - k) / (Rpr * H0 / 1000), 1.0 / 3.0);
			double W0 = 4 + 10 / (mu - 0.1);
			double h0 = H / H0;
			double Wp = W0 * (1 - h0);
			return Wp;
		}

		private void CalculateTRL(double lambda, PointPairList profileWithForest, double distance, WaveRange range)
		{
			double acceptableFading = TrlCalculator.GetAcceptableFading(range.EnergyPotention);
			double mediumAltitude = TrlCalculator.GetMediumAltitude(profileWithForest);
			var station = (TRS)intervalSettings.StationType.SelectedItem;

			int antennHeight = intervalSettings.AntennHeight.Value.Value;
			var firstStation = new PointPair(0, profileList.First().Y + antennHeight);
			PointPair firstPoint;
			double firstAngleClosure = TrlCalculator.GetAngleOfClosure(profileWithForest, Constants.Tension, firstStation, out firstPoint);
			if (firstPoint != null)
			{
				Logger.Log("Первая точка: " + firstPoint.X.ToString("0.00") + " км, " + firstPoint.Y.ToString("0.00") + " м", LoggingLevel.Information);
				graphHolder.DrawFirstAngleLine(firstPoint, antennHeight);

			}

			var secondStation = new PointPair(distance, profileList.Last().Y + antennHeight);
			PointPair secondPoint;
			double secondAngleClosure = TrlCalculator.GetAngleOfClosure(profileWithForest, Constants.Tension, secondStation, out secondPoint);
			if (secondPoint != null)
			{
				Logger.Log("Вторая точка: " + secondPoint.X.ToString("0.00") + " км, " + secondPoint.Y.ToString("0.00") + " м", LoggingLevel.Information);
				graphHolder.DrawSecondAngleLine(secondPoint, antennHeight);
			}

			double fading = TrlCalculator.GetSignalFading(distance, lambda, mediumAltitude, firstAngleClosure + secondAngleClosure, station.AntennGain);
			Logger.Log(
				(acceptableFading - fading > 0 ? "Интервал пригоден\r" : "Интервал непригоден\r") +
				"Величина затухания сигнала на интервале: " + fading.ToString("0.00") + " дБ\rДопустимое затухание сигнала: " +
				acceptableFading.ToString("0.00") + " дБ\r" + "Запас сигнала: " + (acceptableFading - fading).ToString("0.00") +
				" дБ", LoggingLevel.Information);
		}

		/// <summary>
		/// Определение препятствий по точкам пересечения
		/// </summary>
		/// <param name="intersections">Точки пересечения линии крит. просветов с профилем</param>
		/// <returns>Список препятствий</returns>
		private List<Barrier> GetBarriers(PointPairList intersections)
		{
			var barriers = new List<Barrier>();
			for (int i = 0; i < intersections.Count; i += 2)
			{
				double x1 = intersections[i].X;
				double x2 = intersections[i + 1].X;
				PointPair highestPoint = intersections[i];
				for (double x = x1; x <= x2; x += 0.01)
				{
					double y = profileList.SplineInterpolateX(x, Constants.Tension);
					if (y > highestPoint.Y)
						highestPoint = new PointPair(x, y);
				}
				double straightLineY = graphHolder.GetStraightLinePoints().InterpolateX(highestPoint.X);
				double gapsLineY = graphHolder.GetCriticalLinePoints().InterpolateX(highestPoint.X);
				var secant = new PointPairList();
				secant.Add(new PointPair(x1, intersections[i].Y));
				secant.Add(new PointPair(x2, intersections[i + 1].Y));

				PointPair dy = new PointPair();
				for (double x = x1; x <= x2; x += 0.01)
				{
					double yp = profileList.SplineInterpolateX(x, Constants.Tension);
					double y = secant.InterpolateX(x);
					if (yp - y > dy.Y)
						dy = new PointPair(x, yp - y);
				}

				barriers.Add(new Barrier(x1, x2, highestPoint, straightLineY - gapsLineY, straightLineY - highestPoint.Y, dy));
			}
			return barriers;
		}

		/// <summary>
		/// Расчет величины затухания на открытом интервале
		/// </summary>
		/// <returns>Величина затухания</returns>
		private double CalculateFadingOnOpenedInterval()
		{
			double fading = 0;
			PointPair mirrorPoint = RrlCalculator.GetMirrorPoint(profileList, Constants.Tension,
				intervalSettings.AntennHeight.Value.Value);
			if (mirrorPoint == null)
			{
				Logger.Log("Не удалось найти точку отражения", LoggingLevel.Error);
				return 0;
			}

			var antennHeight = intervalSettings.AntennHeight.Value.Value;
			graphHolder.DrawMirrorCurve(mirrorPoint, antennHeight);

			var straightLinePoint = new PointPair(mirrorPoint.X, graphHolder.GetStraightLinePoints().InterpolateX(mirrorPoint.X));
			double H = straightLinePoint.Y - mirrorPoint.Y;
			double H0 = straightLinePoint.Y - graphHolder.GetCriticalLinePoints().SplineInterpolateX(mirrorPoint.X, Constants.Tension);
			Surfaces surface;
			OpenIntReqWindow window = new OpenIntReqWindow();
			window.ShowDialog();
			surface = window.surfaceType;
			fading = RrlCalculator.GetWaveDumpingOpened(surface, H / H0);
			Logger.Log(surface.Description(), LoggingLevel.Information);
			return fading;
		}

		/// <summary>
		/// Расчет величины затухания на полуоткрытом интервале
		/// </summary>
		/// <param name="barriers">Список препятствий</param>
		/// <param name="distance">Длина интервала</param>
		/// <returns>Величина затухания</returns>
		private double CalculateFadingOnHalfOpenedInterval(List<Barrier> barriers, double distance)
		{
			//Объединение препятствий
			int waveNum = intervalSettings.WaveNum.Value.Value;
			var range = (WaveRange)intervalSettings.WaveRange.SelectedItem;
			var lambda = range.GetWaveLength(waveNum);

			for (int i = 0; i < barriers.Count - 1; ++i)
			{
				if (RrlCalculator.IsOneBarrier(lambda, barriers[i], barriers[i + 1]))
				{
					barriers[i + 1].X1 = barriers[i].X1;
					if (barriers[i].HighestPoint.Y > barriers[i + 1].HighestPoint.Y)
					{
						barriers[i + 1].HighestPoint = barriers[i].HighestPoint;
						barriers[i + 1].H = barriers[i].H;
						barriers[i + 1].H0 = barriers[i].H0;
					}
					barriers[i] = null;
				}
			}

			return barriers.Where(b => b != null).Sum(barrier => RrlCalculator.GetWaveDumpingHalfOpened(barrier, distance));
		}

		/// <summary>
		/// Объединение профиля с лесом в единую кривую
		/// </summary>
		/// <returns>Список точек объединной кривой</returns>
		private PointPairList MergeProfileWithForest()
		{
			var mergedPoints = new PointPairList(profileList);
			foreach (var forestPoints in graphHolder.GetForests())
			{
				double startX = forestPoints.First().X - 0.01;
				double startY = mergedPoints.SplineInterpolateX(startX, Constants.Tension);
				double finishX = forestPoints.Last().X + 0.01;
				double finishY = mergedPoints.SplineInterpolateX(finishX, Constants.Tension);
				var pointsForDelete = mergedPoints.Where(mergedPoint => mergedPoint.X > startX && mergedPoint.X < finishX).ToList();
				foreach (var point in pointsForDelete)
				{
					mergedPoints.Remove(point);
				}
				mergedPoints.Add(startX, startY);
				mergedPoints.AddRange(forestPoints);
				mergedPoints.Add(finishX, finishY);
				mergedPoints.Sort(SortType.XValues);
			}
			return mergedPoints;
		}

		#endregion


		#region Logging

		private void WriteMessageToTextbox(string message, LoggingLevel level)
		{
			var textRange = new TextRange(rtbOutput.Document.ContentStart, rtbOutput.Document.ContentEnd);
			textRange.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Normal);

			textRange = new TextRange(rtbOutput.Document.ContentStart, rtbOutput.Document.ContentStart) { Text = new string('-', 20) + "\r" };
			textRange.ApplyPropertyValue(RichTextBox.ForegroundProperty,
				new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Black));

			textRange = new TextRange(rtbOutput.Document.ContentStart, rtbOutput.Document.ContentStart) { Text = message + "\r" };
			textRange.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold);
			textRange.ApplyPropertyValue(RichTextBox.ForegroundProperty,
				level == LoggingLevel.Error
					? new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Red)
					: new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Black));
			rtbOutput.ScrollToEnd();
		}

		#endregion

		private void MapBtn_OnClick(object sender, RoutedEventArgs e)
		{
			MapWindow map = new MapWindow(gciFirstStation.Location, gciSecondStation.Location);
			map.ShowDialog();
			gciFirstStation.Location = map.FirstLocation;
			gciSecondStation.Location = map.SecondLocation;
		}

		private void LoadMenuItem_Click(object sender, RoutedEventArgs e)
		{
			var project = SaverLoader.LoadProject();
			if (project == null) return;

			try
			{
				if (project.FirstStationLocation != null && project.SecondStationLocation != null)
				{
					tabOnline.IsSelected = true;
					gciFirstStation.Latitude = new GeoCoord(project.FirstStationLocation.Latitude);
					gciFirstStation.Longitude = new GeoCoord(project.FirstStationLocation.Longitude);
					gciSecondStation.Latitude = new GeoCoord(project.SecondStationLocation.Latitude);
					gciSecondStation.Longitude = new GeoCoord(project.SecondStationLocation.Longitude);
				}
				else
				{
					tabOffline.IsSelected = true;
					iudIntervalLength.Value = (int)project.Profile.Last().X;
					iudFirstStationHeight.Value = (int)project.Profile.First().Y;
					iudSecondStationHeight.Value = (int)project.Profile.Last().Y;
				}
				profileList = project.Profile;
				forestList = project.Forests;
				intervalSettings.StationType.SelectValueByHeader(project.Station.Name);
				intervalSettings.AntennHeight.Value = project.AntennHeight;
				intervalSettings.WaveRange.SelectValueByHeader(project.WaveRange.Name);
				intervalSettings.WaveNum.Value = project.WaveNum;
				RebuildGraph();
				OnProfileListChanged?.Invoke();
				OnForestListChanged?.Invoke();
				graphHolder.DrawForests(forestList);
				graphHolder.InitMaxScaleY();
				BtnCalculate_OnClick(null, null);
				Logger.Log("Файл успешно загружен", LoggingLevel.Information);
			}
			catch
			{
				Logger.Log("Файл поврежден, не удалось прочитать данные", LoggingLevel.Error);
			}
		}

		private void SaveMenuItem_Click(object sender, RoutedEventArgs e)
		{
			GeoPoint firstStationLocation = null;
			if (gciFirstStation.Location != null)
				firstStationLocation = new GeoPoint(gciFirstStation.Latitude.ToDouble(), gciFirstStation.Longitude.ToDouble(), 0);
			GeoPoint secondStationLocation = null;
			if (gciSecondStation.Location != null)
				secondStationLocation = new GeoPoint(gciSecondStation.Latitude.ToDouble(), gciSecondStation.Longitude.ToDouble(), 0);
			var profile = profileList;
			var forests = forestList;
			var station = (Station)intervalSettings.StationType.SelectedItem;
			var antennHeight = intervalSettings.AntennHeight.Value;
			var waveRange = (WaveRange)intervalSettings.WaveRange.SelectedItem;
			var waveNum = intervalSettings.WaveNum.Value;
			if (profile == null || forests == null || station == null || antennHeight == null
				|| waveRange == null || waveNum == null)
			{
				Logger.Log("Заданы не все параметры интервала", LoggingLevel.Warning);
				return;
			}
			ProjectContainer project = new ProjectContainer(firstStationLocation, secondStationLocation, profile,
				forests, station, antennHeight.Value, waveRange, waveNum.Value);
			SaverLoader.SaveProject(project);
		}
	}
}